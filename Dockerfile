# Build container
FROM maven:3.5-jdk-8 as BUILD

COPY . /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package -DskipTests=true

# Application container
FROM openjdk:8-jdk-slim
VOLUME /tmp

COPY --from=BUILD /usr/src/app/target/app.jar app.jar

ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar